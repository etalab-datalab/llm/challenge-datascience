import re
import dotenv
import os
import requests
import logging
import asyncio
from openai import OpenAI

from prompts import *
from retrieval import search_db, find_official_sources, create_web_collection, search_tmp_rag

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

dotenv.load_dotenv('.env')

WRITER_MODEL = '<ID of the writers>'
SUPERVISOR_MODEL = '<ID of the supervisor>'

session = requests.session()
ALBERT_KEY = os.getenv('ALBERT_KEY')
ALBERT_URL = os.getenv('ALBERT_URL')
session.headers = {"Authorization": f"Bearer {ALBERT_KEY}"}

user = "XXX"

def get_completion(model, prompt, user, temperature, max_tokens, history = []):
    data = {
        "model": model,
        "messages": history[-5:] + [{"role": "user", "content":prompt}],
        "stream": False,
        "n": 1,
        "temperature": temperature,
        "max_tokens":max_tokens,
        "user": user, # to start a conversation with chat history, setup a user name
    }
    response = session.post(url=f"{ALBERT_URL}/chat/completions", json=data)
    return response.json()['choices'][0]['message']['content']

async def get_completion_async(model, prompt, user, temperature, max_tokens, history):
    return await asyncio.to_thread(get_completion, model, prompt, user, temperature, max_tokens, history)


def extract_number(string):
    #string = string.strip()
    match = re.search(r'\b[0-4]\b', string)
    if match:
        return int(match.group())
    else:
        return None  # or handle the case when no number is found

def get_ragger_choice(question, docs, retries=0, max_retries=3, history=[]):
    """
    Retrieves a user's choice by generating a response from a model, 
    ensuring the result is a valid integer. Retries on failure.

    Parameters:
    question (str): The question to pose.
    docs (list): List of documents to assist in generating the prompt.
    retries (int): Current retry count (default is 0).
    max_retries (int): Maximum allowed retries before giving up (default is 3).

    Returns:
    int: The validated integer choice from the model or 0 if retries exceeded.
    """
    try:
        # Get the model's completion based on the question and docs
        prompt = get_prompt_ragger(question, docs)
        response = get_completion(SUPERVISOR_MODEL, prompt, user, temperature=0.2, max_tokens=3, history=history)
        print(prompt)

        # Attempt to extract and return an integer from the model's response
        choice = int(extract_number(response))
        return choice

    except ValueError as e:
        # Handle failure to convert response to integer
        logger.warning(f"Invalid response: expected integer, got '{response}'. Error: {e}")
        retries += 1

        if retries >= max_retries:
            logger.error(f"Exceeded maximum retries ({max_retries}). Returning default value 0.")
            return 0
        else:
            logger.info(f"Retrying... Attempt {retries + 1} of {max_retries}.")
            return get_ragger_choice(question, docs, retries=retries, max_retries=max_retries)

    except Exception as e:
        # Catch any other unexpected errors
        logger.exception(f"Unexpected error: {e}")
        return 0

def get_teller_answer(question, context, choice):
    prompt = get_prompt_teller(question, context, choice)
    answer = get_completion(WRITER_MODEL, prompt, user, temperature=0.2, max_tokens=600)
    return answer

def get_checker_answer(question, response, refs):
    prompt = get_prompt_checker(question, response, refs)
    answer = get_completion(WRITER_MODEL, prompt, user, temperature=0.2, max_tokens=600)
    return answer

def get_googleizer_answer(question):
    answer = get_completion(WRITER_MODEL, get_prompt_googleizer(question), user, temperature=0.2, max_tokens=100)
    return answer

def get_final_answer(question, answers, history):
    prompt = get_prompt_concat_answer(answers, question)
    answer =get_completion(SUPERVISOR_MODEL, prompt, user, temperature=0.2, max_tokens=1024, history=history)
    return answer

def get_list_web_sources(question):
    results = find_official_sources(question, n=5)
    results = "\n".join([f'• :url_start:{site['title']} ---- {site['href']}:url_end:\nExtrait : "{site['body']}"\n' for site in results])
    answer = f"Voilà ce que j'ai trouvé sur internet parmi les sources officielles de l'État :\n\n{results}"
    return answer

async def teller_multi_stuffs(prompts, history):
    tasks = []
    for prompt in prompts:
        task = asyncio.create_task(get_completion_async(WRITER_MODEL, prompt, user, temperature=0.2, max_tokens=200, history=history))
        tasks.append(task)
    answers = await asyncio.gather(*tasks)
    return answers

def get_initial_docs(docs, refs, n, fact, question, collections_wanted):
    """
    Retrieve or slice docs and refs based on current retry iteration.
    """
    if not docs:
        docs, refs = search_db(question, k=25, collections_wanted=collections_wanted)  # First doc search
    docs_tmp = docs[n*fact:(n+1)*fact]
    refs_tmp = refs[n*fact:(n+1)*fact]
    return docs, refs, docs_tmp, refs_tmp

def format_context(docs_tmp, refs_tmp):
    """
    Truncate docs and prepare them for supervisor.
    """
    if docs_tmp:
        context = "\n-------\n".join([doc[:250]+"..." for doc in docs_tmp])  # Truncate docs
        context_refs = refs_tmp
        return context, context_refs
    else:
        return  "", []

def determine_choice(question, context, history, netsearch):
    """
    Determine the user's choice (local search or web search).
    """
    if question.lower().strip().startswith(("web", "internet")) or netsearch:
        return 4  # Web search
    return get_ragger_choice(question, context, history)

async def retry_pipeline(question, docs, refs, n, fact, callback_handler, history, max_retry):
    """
    Handle retry logic for document searches when local documents are insufficient.
    """
    if callback_handler:
        callback_handler.print_tmp(f"Je ne trouve pas de documents, je cherche encore...{'.' * n}")
    else:
        logger.info(f"Retrying {n}")
    
    n += 1
    return await go_pipeline(question, docs=docs, refs=refs, n=n, fact=fact, callback_handler=callback_handler, history=history, max_retry=max_retry)

def choice_1_2_handler(choice, callback_handler):
    if choice ==1 : 
        if callback_handler: callback_handler.print_tmp("J'ai trouvé des choses !")
        else: logger.info("J'ai trouvé des choses !")
    elif callback_handler : callback_handler.print_tmp("Je pense pouvoir répondre de moi même...")
    else: logger.info("Je pense pouvoir répondre de moi même...")

def custom_prompt(question, collections_wanted):
    if "decisions-adlc" in collections_wanted:
        question = question + " nommes les numéros de décisions utilisés, numéros de paragraphe et/ou les textes de Loi s'il y en a dans ta réponse."
    return question

def custom_websearch(question, google_search, collections_wanted, callback_handler):
    n_results = None
    if callback_handler: callback_handler.print_tmp("Je cherche sur internet...")
    else: print("Je cherche sur internet...")
    # for custom collection
    if question.startswith("adlc") or "decisions-adlc" in collections_wanted:
        google_search = '"Décision n°" ' + google_search + ' site:www.autoritedelaconcurrence.fr filetype:pdf'
        n_results = 1
    logger.info(f"{question} ---->{google_search}")
    return google_search, n_results

# Pipeline recursif
async def go_pipeline(question, docs=[], refs=[], n=0, fact=5, callback_handler=None, history=None, max_retry=5, netsearch=False, collections_wanted=[]):
    if n==0: logger.info(f"---- {question} ----")
    docs, refs, docs_tmp, refs_tmp = get_initial_docs(docs, refs, n, fact, question, collections_wanted)
    context, context_refs = format_context(docs_tmp, refs_tmp)
    choice = determine_choice(question, context, history, netsearch)
    logger.info(f"CHOIX: [{choice}{peter_explain[choice]} - {n}]")

    # Choice 0,3 but docs 
    if choice in [0,3] and n<max_retry: #len(docs)>=1
        #n+=1
        return await retry_pipeline(question, docs, refs, n, fact, callback_handler, history, max_retry)
    if choice in [1, 2]:
        choice_1_2_handler(choice, callback_handler)
        question = custom_prompt(question, collections_wanted)
        prompts = get_prompt_teller_multi(question, docs_tmp, choice)
        answers = await teller_multi_stuffs(prompts, history)
        answer = get_final_answer(question, answers, history)
        # Adding refs
        if choice == 1:
            ref_answer = get_checker_answer(question, answer, "\n".join(context_refs))
        else:
            ref_answer = "🤖"#"Je n'ai pas utilisé de sources pour cette réponse."

    if choice == 4 or n==max_retry: # too much retry ? go internet 
        choice = 4
        google_search = get_googleizer_answer(question)
        google_search, n_results = custom_websearch(question, google_search, collections_wanted, callback_handler)
        web_results = find_official_sources(google_search)[:n_results]
        if web_results == []:
            return "Désolé je n'ai rien trouvé à ce sujet, ni dans les documents de l'État, ni sur internet !", ""
        if callback_handler: callback_handler.print_tmp("Je classe ça...")
        else: logger.info("Je classe ça...")
        try:
            create_web_collection(web_results)
            docs_tmp = search_tmp_rag(question)
        except Exception as e:
            docs_tmp = []
            logger.info("Il y a eu un problème...")
            if callback_handler: callback_handler.print_tmp("Il y a eu un problème...")

        if callback_handler:callback_handler.print_tmp("Je lis tout ça...")
        prompts = get_prompt_teller_multi(question, docs_tmp, choice)

        if callback_handler: callback_handler.print_tmp("Je mets ça en forme...")
        logger.info("Je mets ça en forme...")
        answers = await teller_multi_stuffs(prompts, history)
        answer = get_final_answer(question, answers,history)
        ref_answer = "\n".join([f'• :url_start:{site['title']} ---- {site['href']}:url_end:\nExtrait : "{site['body']}"\n' for site in web_results])
    return answer, ref_answer

