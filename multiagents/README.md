[![N|Solid](https://upload.wikimedia.org/wikipedia/fr/2/2f/Logo-etalab-320x200.png?20160804051056)]()

# MultiAgents

Le PoC multiagents est présenté dans [cet article](https://medium.com/@camille_andre/multi-agents-faire-du-tooling-avec-les-mains-f004a4550c55).

## Installation
Créez un venv et installez les dépendances nécessaires.
```sh
python3 -m venv venv
venv/bin/activate
pip install -r requirements.txt
```

Ajoutez vos clés api pour les modèles et votre base qdrant dans un .env que vous créez. Les variables d'environnement à ajouter sont : 
- QDRANT_KEY, QDRANT_URL : pour la base Qdrant
- EMBEDDINGS_URL, EMBEDDINGS_KEY : Pour le modèle d'embedding
- LLAMA_URL, LLAMA_API_KEY : Pour le "petit" modèle
- MISTRAL_URL, MISTRAL_API_KEY : Pour le "gros" modèle

Un streamlit est disponible, adapté de [snowchat](https://snowchat.streamlit.app/?utm_medium=oembed).

Pour lancer le streamlit avec les modèles :
```sh
streamlit run app.py
```