# Albert

Albert est un assistant du service public. Son but est de vous aider à trouver des réponses pour toutes vos questions administratives.

## Sources

- **Fiches travail**: Documents issus de travail-emploi.gouv.fr
- **Vos droits**: Les fiches de service-public.fr
- **Décisions ADLC**: Décisions de l'Autorité de la Concurrence

Voici quelques exemples de questions à poser à Albert : 

- Comment refaire une pièce d'identité nationale ?
- Un contrat de CDI doit-il être forcément écrit ?
- Peut-on avoir des jours de congés pour un mariage ?
- Donnes moi des références sur le vote par procuration