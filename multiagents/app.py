import re
import warnings
import asyncio
from tools import *
from retrieval import collection_names

import streamlit as st

# from utils.snow_connect import SnowflakeConnection
from snowchat_ui import StreamlitUICallbackHandler, message_func
#from utils.snowddl import Snowddl

warnings.filterwarnings("ignore")

st.set_page_config(layout="wide",     page_title="Albert",
    page_icon="🧊")

chat_history = []

gradient_text_html = """
<style>
.gradient-text {
    font-weight: bold;
    background: -webkit-linear-gradient(left, #0349fc, #9193fa);
    background: linear-gradient(to right, #0349fc, #9193fa);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    display: inline;
    font-size: 3em;
}
</style>
<div class="gradient-text">Albert expérimental</div>

"""


st.markdown(gradient_text_html, unsafe_allow_html=True)

st.caption("Service-Public.fr, Travail-emploi.fr, Autoritedelaconcurrence.fr")

model = 'toto'
st.session_state["model"] = 'toto'
st.session_state["netsearch"]: False

with open("styles.md", "r") as styles_file:
    styles_content = styles_file.read()
with open("sidebar.md", "r") as sidebar_file:
    sidebar_content = sidebar_file.read()

st.sidebar.markdown(sidebar_content)

if "toast_shown" not in st.session_state:
    st.session_state["toast_shown"] = False

if "rate-limit" not in st.session_state:
    st.session_state["rate-limit"] = False


# Show a warning if the model is rate-limited
if st.session_state["rate-limit"]:
    st.toast("Probably rate limited.. Go easy folks", icon="⚠️")
    st.session_state["rate-limit"] = False

if st.session_state["model"] == "Mixtral 8x7B":
    st.warning("This is highly rate-limited. Please use it sparingly", icon="⚠️")

INITIAL_MESSAGE = [
    {"role": "user", "content": "Bonjour!"},
    {
        "role": "assistant",
        "content": """Bonjour ! Je suis Albert, et je peux vous aider si vous avez des questions administratives ! 🔍
        
        Je suis connecté à trois bases de connaissances : 
        • Fiches Service-Public
        • Fiches Travail-Emploi
        • Décisions ADLC
        
        Je peux également chercher sur les sites officiels de l'État, pour ça activez la recherche à gauche ou commencez votre message par quelque chose comme "web", "internet" ou encore "cherches sur internet" suivi de votre question.
        
        Comment puis-je vous aider ?
        """,
    },
]




if st.sidebar.toggle("Rechercher exclusivement sur internet", help="Tips :\n1) Albert ne recherches que les sites officiels de l'État et wikipedia.\n2) Pour rechercher en ligne parmi les documents pdf de l'ADLC, ajoutez \"adlc\" en plus devant votre message (traitement un peu plus long)."):
    st.session_state["netsearch"]= True
    collections_wanted = st.sidebar.multiselect(
        "Collections disponibles",
        collection_names,
        ["fiches-travail", "fiches-vos-droits"], disabled=True)
else:
    st.session_state["netsearch"]= False
    collections_wanted = st.sidebar.multiselect(
    "Collections",
    collection_names,
    ["fiches-travail", "fiches-vos-droits"], disabled=False)

if "netsearch" not in st.session_state.keys():
    st.session_state["netsearch"]= False

# Add a reset button
if st.sidebar.button("Reset Chat"):
    for key in st.session_state.keys():
        del st.session_state[key]
    st.session_state["messages"] = INITIAL_MESSAGE
    st.session_state["history"] = []

st.sidebar.markdown("""
<br><p style="font-size: 12px; text-align: center;"><i>Je peux me tromper, en cas de doute pensez à toujours vérifier mes sources et mes réponses.</i></p>
""", unsafe_allow_html=True)

st.write(styles_content, unsafe_allow_html=True)

# Initialize the chat messages history
if "messages" not in st.session_state.keys():
    st.session_state["messages"] = INITIAL_MESSAGE

if "history" not in st.session_state:
    st.session_state["history"] = []

if "model" not in st.session_state:
    st.session_state["model"] = model

# Prompt for user input and save
if prompt := st.chat_input():
    st.session_state.messages.append({"role": "user", "content": prompt})

for message in st.session_state.messages:
    message_func(
        message["content"],
        True if message["role"] == "user" else False,
        True if message["role"] == "data" else False,
        model,
    )

callback_handler = StreamlitUICallbackHandler(model)


def append_chat_history(question, answer):
    st.session_state["history"].append((question, answer))


def append_message(content, role="assistant"):
    """Appends a message to the session state messages."""
    if content.strip():
        st.session_state.messages.append({"role": role, "content": content})



if (
    "messages" in st.session_state
    and st.session_state["messages"][-1]["role"] != "assistant"
):
    user_input_content = st.session_state["messages"][-1]["content"]

    if isinstance(user_input_content, str):
        callback_handler.start_loading_message()

        #result = chain.invoke(
        #    {
        #        "question": user_input_content,
        #        "chat_history": [h for h in st.session_state["history"]],
        #    }
        #)
        answer, refs = asyncio.run(go_pipeline(user_input_content, docs=[], refs=[], n=0, fact=5, callback_handler=callback_handler, history = st.session_state.messages, netsearch=st.session_state["netsearch"], collections_wanted=collections_wanted))
        answer = answer+"\n\n"+refs
        answer = answer.strip()
        #append_message(answer + refs)
        print(answer+refs)
        callback_handler.put_response(answer)
    st.session_state.messages.append({"role": "assistant", "content": answer})

if (
    st.session_state["model"] == 'toto'
    and st.session_state["messages"][-1]["content"] == ""
):
    st.session_state["rate-limit"] = True
