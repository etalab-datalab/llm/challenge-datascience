import numpy as np

from nltk.corpus import stopwords
from langchain.embeddings import HuggingFaceEmbeddings
from langchain_core.output_parsers import StrOutputParser
from langchain_core.prompts import PromptTemplate
from sklearn.feature_extraction.text import TfidfVectorizer
from langchain_community.llms import LlamaCpp
from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler

french_stopwords = set(stopwords.words('french'))
new_stopwords = {"s\'", "quel", "que", "quoi", "comment", "l\'", "d\'", 'mais', 'ou', 'et', 'donc', 'or', 'ni', 'car', 'quelle', 'quelles', 'pourquoi'}
french_stopwords.update(new_stopwords)

embedder = HuggingFaceEmbeddings(model_name="intfloat/e5-large")

callback_manager = CallbackManager([StreamingStdOutCallbackHandler()])
model_path = "your_path_here"

def load_llm():
    return LlamaCpp(
    model_path=model_path,
    n_gpu_layers=-1,
    n_batch=1048,
    n_ctx=4096,
    f16_kv=True,  # MUST set to True, otherwise you will run into problem after a couple of calls
    verbose=False,
    echo = False,
    max_tokens = -1,#callback_manager=callback_manager,
)

prompt = PromptTemplate.from_template(
    """
<|im_start|>system
Tu es un assistant administratif, tu réponds de manière polie et succinte.<|im_end|>
<|im_start|>user
En t'inspirant des articles ci-dessous, réponds à la question suivante et commence en citant obligatoirement le numéro d'article que tu as utilisé pour ta réponse à la fin : {question} \n
Si tu ne connais pas la réponse, dis simplement que tu n'as pas assez d'éléments pour pouvoir répondre. Ta réponse dois être courte.
Voilà les articles qui contiennent des éléments de réponses : {article}<|im_end|>
<|im_start|>assistant
"""
)

prompt_create_questions = PromptTemplate.from_template(
    """
<|im_start|>system
Tu es un assistant administratif.<|im_end|>
<|im_start|>user
En t'inspirant du texte ci-dessous, réponds uniquement avec une liste de 2-3 questions très précises et détaillées que l'on pourrait poser sur ce texte et dont la réponse se trouve dans ce texte.
Si le texte est très court ne réponds qu'avec une ou deux questions. N'inventes pas des questions qui ne veulent rien dire, si tu n'as qu'une seule question c'est très bien aussi.
Les questions ne doivent pas être vague, et pouvoir être associées très facilement à ce texte si on les comparait à d'autres questions pour d'autres textes.
Si une question porte sur l'article de manière générale, le numéro ou l'id de l'article doit être contenu dans la question.
Les questions doivent contenir les éléments importants comme des dates ou des numéros si besoin.
Voici des exemples pour t'aider:
Mauvaises questions qu'on ne veut pas :  ["De quoi parle ce texte ?", "Qui est mentioné ici ?"] (trop vague)
Bonne question : ["Quel groupe est concerné par la décision XX-XXX-XX ?"] (net et précis)
Voilà le texte : {article}<|im_end|>
<|im_start|>assistant
"""
)
def remove_french_stopwords(text):
    text = text.lower()
    tokens = text.split()  # Split text into words
    filtered_tokens = [token for token in tokens if token.lower() not in french_stopwords]
    return ' '.join(filtered_tokens)

def extract_keywords_tfidf(docs, corpus, top_n=5):
    """
    Extracts the top N keywords from a given document or list of documents using TF-IDF.
    
    Parameters:
    - docs (str or list of str): A single document or a list of documents.
    - top_n (int): Number of top keywords to extract from each document.
    
    Returns:
    - list of (str, float) tuples: A list of (keyword, score) tuples for each document.
    """
    if isinstance(docs, str):
        docs = [docs]
    
    # Initialize the TF-IDF vectorizer
    vectorizer = TfidfVectorizer(stop_words=list(french_stopwords))
    
    # Fit and transform the documents
    model = vectorizer.fit(corpus) #
    tfidf_matrix = model.transform(docs)
    # Get feature names to access the corresponding columns in the matrix
    feature_names = np.array(vectorizer.get_feature_names_out())
    # Initialize a list to hold the results
    keywords_list = []
    
    # Iterate through each document
    for doc_idx in range(tfidf_matrix.shape[0]):
        # Get the row corresponding to the document
        row = np.squeeze(tfidf_matrix[doc_idx].toarray())
        # Get the indices of the top N values
        top_n_indices = row.argsort()[-top_n:][::-1]
        # Extract the corresponding keywords and scores
        keywords = [feature_names[i] for i in top_n_indices] #[(feature_names[i], row[i]) for i in top_n_indices]
        # Add to the list of results
        keywords_list.append(keywords)
    # If only one document was processed, return its keywords directly
    if len(docs) == 1:
        return keywords_list[0]
    
    return keywords_list

def ensure_list_length(lst, target_length=5):
    # Calculate the number of elements to add
    elements_to_add = target_length - len(lst)
    # Extend the list with None for any missing elements, if necessary
    if elements_to_add > 0:
        lst.extend([None] * elements_to_add)
    return lst

# Chain
def format_docs(input_stuff):
    if len(input_stuff['article']) == 0:
        return []
    return input_stuff['article']

def format_question(input_stuff):
    return input_stuff["question"]

def run_question_rag(llm, question, docs, prompt):
    chain = {"question": format_question, "article": format_docs} | prompt | llm | StrOutputParser()
    answer = chain.invoke({"question": question, "article":docs})
    return answer
