# challenge-datascience

Les projets présentés ici contiennent les codes des PoCs liés aux articles d'Etalab. Ces projets ne sont pas voués à partir en production tels quels, mais plutôt à être utilisés comme briques pour d'autres projets. Tous les scripts présents ici sont utilisables librement.